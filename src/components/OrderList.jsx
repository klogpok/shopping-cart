import React, {Component} from 'react';
import { CSSTransitionGroup } from 'react-transition-group'

import OrderItem from './OrderItem';
import TotalValue from './TotalValue';

class OrderList extends Component {

    handlerDeleteOrder = (id) => {
        this.props.handlerDeleteOrder(id);
    }

    handlerChangeQty = (id, sign) => {
        this.props.handlerChangeQty(id, sign);
    };

    render() {

        let orders = this.props.data.map((order, index) => {
            return <OrderItem 
                item={order} 
                key={order.id} 
                handlerDeleteOrder = {this.handlerDeleteOrder}
                handlerChangeQty = {this.handlerChangeQty}
            />
        });

        let totalValue = this.props.data.reduce((acc, order) => {
            return acc + order.price * order.qty;
        },0)
        
        return (
            <ul className="product__list">
                <CSSTransitionGroup
                    transitionName="example"
                    transitionAppear={true}
                    transitionAppearTimeout={500}
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={500}>                                      
                        {orders}
                        <TotalValue data={totalValue}/>                        
                </CSSTransitionGroup>              
            </ul>
        )
    }
}

export default OrderList;