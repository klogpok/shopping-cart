import React from 'react';

const TotalValue = (props) => {
    
    return (
        <li className="product__list-total">
            <div className="total-value">
                Total amount:
                <span>${props.data}</span>
            </div>
        </li>
    );
};

export default TotalValue;