import React, {Component} from 'react';

class OrderItem extends Component {

    constructor(props) {
        super(props); 
        this.state = {
        }
    }

    handlerDeleteOrder = (e) => {
        e.preventDefault();
        this.props.handlerDeleteOrder(this.props.item.id);
    }

    handlerChangeQty = (sign, e) => {
        e.preventDefault();
        this.props.handlerChangeQty(this.props.item.id, sign);
    }

    render() {

        return (
            <li className="product__list-item">
                <div className="product__list-item__image">
                    <img src={this.props.item.imgUrl} alt="" />
                </div>
                <div className="product__list-item__desc">
                    <div className="product__list-item__desc-name">{this.props.item.name}</div>
                    <div className="product__list-item__desc-price">${this.props.item.price}/{this.props.item.unit}</div>
                </div>
                <div className="product__list-item__qty">
                    <a href="" className="product__list-item__qty-minus" onClick={this.handlerChangeQty.bind(this,'-')}>
                        <i className="fa fa-minus"></i>                     
                    </a>
                    <span className="product__list-item__qty-value">{this.props.item.qty}</span>
                    <a href="" className="product__list-item__qty-plus" onClick={this.handlerChangeQty.bind(this,'+')}>
                        <i className="fa fa-plus"></i>
                    </a>
                </div>
                <div className="product__list-item__price">
                    <div className="product__list-item__price-value">${this.props.item.price * this.props.item.qty}</div>
                </div>
                <a href="" className="product__list-item__deleteBtn" onClick={this.handlerDeleteOrder}>
                    <i className="fa fa-times"></i>
                </a>
            </li>
        )
    }    
}

export default OrderItem;