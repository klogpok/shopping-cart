import React from 'react';

const Header = () => {
    return (
        <div className="header">
            <div className="header__title">Shopping List</div>
            <div className="header__list">
                <ul>
                    <li className="header__list-item"><span>items</span></li>
                    <li className="header__list-item"><span>quantity</span></li>
                    <li className="header__list-item"><span>price</span></li>
                </ul>
            </div>
        </div>
    );
};

export default Header;