import React, { Component } from 'react';
import './App.css';

import Header from './components/Header';
import OrderList from './components/OrderList';

const orders = [
    { id: 1, name: 'Oranges', price: 0.5, unit: 'piece', qty: 1, imgUrl: 'img/orange3.png'},
    { id: 2, name: 'Lettuce', price: 21, unit: 'piece', qty: 1, imgUrl: 'img/letucce.png'},
    { id: 3, name: 'Strawberries', price: 4, unit: 'kg', qty: 1, imgUrl: 'img/strawberry.png'}
];


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      orderList: []
    }
  }

  handlerDeleteOrder = (id) => {
      const orders = this.state.orderList.filter( order => order.id !== id);
      this.setState({orderList: orders});
  }

  handlerChangeQty = (id, sign) => {
      console.log(id, sign);

      const updatedOrders = this.state.orderList.map( order => {
        if (order.id === id) {
            let newValue = null;

            if (order.qty <= 1 && sign === '-') {
                return order;
            } else {
                newValue = sign === '+' ? order.qty + 1 : order.qty - 1;
                const updatedOrder = {...order};
                updatedOrder.qty = newValue;
                return updatedOrder;
            }
        }
        return order;
      });

      this.setState({orderList: updatedOrders});
  }

  emptyAllOrders = (e) => {
      e.preventDefault();
      this.setState({orderList: []});    
  }

  componentDidMount = () => {
      this.setState({orderList: orders});
  }

  render() {
    return (
      <div className="App">
        <Header />
        <OrderList 
            data={this.state.orderList} 
            handlerDeleteOrder = {this.handlerDeleteOrder}
            handlerChangeQty = {this.handlerChangeQty}
        />
        <div className="buttons">
            <a href="" className="btn green">Add item</a>
            <a href="" className="btn grey" onClick={this.emptyAllOrders}>Empty list</a>
        </div>       
      </div>
    );
  }
}

export default App;
